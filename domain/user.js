module.exports = class User {
  constructor(obj) {
    obj = obj || {};
    this.id = obj.id;
    this.username = obj.username;
    this.password = obj.password;
    this.documentNumber = obj.documentNumber;
    this.paternalLastName = obj.paternalLastName;
    this.maternalLastName = obj.maternalLastName;
  }
}