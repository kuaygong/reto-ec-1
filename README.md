# Servicio Web de Registro de Usuario
## Api Gateway
**URL:** https://j9l3sqhto8.execute-api.us-east-1.amazonaws.com/DEV/users
**METODO HTTP:** POST
**BODY:**
`{
    "username": "test_p",
    "password": "test_p",
    "name": "test_p",
    "documentNumber": "test_p",
    "paternalLastName": "test_p",
    "maternalLastName": "test_p"
}`

![Semantic description of image](/image/image_1.PNG "Image Title")
