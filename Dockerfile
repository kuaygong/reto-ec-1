FROM public.ecr.aws/lambda/nodejs:12

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install 
RUN npm install -g serverless
COPY . .
RUN chmod +x ./scripts/commands.sh
ENTRYPOINT ["./scripts/commands.sh"]