'use strict';
const promisePool = require('./connection/connection');
const User = require('./domain/user');

module.exports.hello = async (event) => {

  const requestBody = JSON.parse(event.body);
  const {username, password, name, documentNumber, paternalLastName, maternalLastName} = requestBody;

  const user = new User();

  user.username = username;
  user.password = password;
  user.name = name;
  user.documentNumber = documentNumber;
  user.paternalLastName = paternalLastName;
  user.maternalLastName = maternalLastName;

  try {
    const [rows,fields] = await promisePool.query(`INSERT INTO usuario(username,password,numero_documento,nombre,apellido_paterno,apellido_materno) VALUES('${user.username}','${user.password}','${user.documentNumber}','${user.name}','${user.paternalLastName}','${user.maternalLastName}')`);

    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: 'Se registro usuario correctamente',
        }
      ),
    };

  } catch(e) {
    return {
      statusCode: 500,
      body: JSON.stringify(
        {
          message: e.message,
        }
      ),
    };
  }
};
